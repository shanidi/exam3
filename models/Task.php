<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property int $urgency
 */
class Task extends \yii\db\ActiveRecord
{

        public function behaviors() {
        return [
            BlameableBehavior::className(),
            
               [
            'class' => TimestampBehavior::className(),
            'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => 'updated_at',
            'value' => new Expression('NOW()'),
        ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['urgency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urgency' => 'Urgency',
            'created_by' => 'Created_by'
           
        ];
    }

//---------------------------------------------------------------------

       public function getUser()
    {
      return $this->hasOne(User::className(),['id'=>'created_by']); 
    }
        public function getUrgencys()
    {
      return $this->hasOne(Urgency::className(),['id'=>'urgency']); 
    }
}
