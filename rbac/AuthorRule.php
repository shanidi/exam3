<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\Post;

/**
 * Checks if authorID matches user passed via params
 */
class AuthorRule extends Rule
{
    public $name = 'editMyUser'; // השם של החוק

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params) // תמיד יכיל 3 פרמטרים, היוזר עובר אוטומטית, אייטם מתייחס לאובייקט הספציפי-אותו חוק יכוללהתייחס לכמה אובייקטים, פרמטר-אנחנו מעבירים לחוק את הפרמטרים שנרצה שהוא ישתמש בהם
    {
        return isset($params['post']) ? $params['post']->id == $user : false; //לוגיקה- אם הקריאייטביי שווה ליוזר תחזיר טרו
        // בודקים שבגלל קיים ארטיקל (לפני הסימן שאלה) אם הוא קיים משיכים ואם לא מחזירים פולס
        //אם זיהנו את המאמר נבדוק מי יצר אותנו 
        //חשוב שהקריאייטד ביי יהיה רשום כמו שאנחנו קראנו לו
    }
}
