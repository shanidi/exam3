<?php

use yii\db\Migration;

/**
 * Class m180624_081757_init_rbac
 */
class m180624_081757_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
             //-------------------------------------תפקידים----------------------------------
        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        
        $employee = $auth->createRole('employee');
        $auth->add($employee);
  
          //-----------------------------הגדרת ילדים ובנים---------------------------------
        $auth->addChild($admin, $employee);
        //------------------------------------הרשאה----------------------------------------
        $editMyUser = $auth->createPermission('editMyUser'); //עובד עם חוק
        
        $editUser = $auth->createPermission('editUser'); //מנהל
        $auth->add($editUser);
        
        $veiwUser = $auth->createPermission('veiwUser'); //עובד
        $auth->add($veiwUser);
        
        $veiwUsers = $auth->createPermission('veiwUsers'); //עובד
        $auth->add($veiwUsers);


        $createTask = $auth->createPermission('createTask'); //עובד+
        $auth->add($createTask);

        $editTask = $auth->createPermission('editTask'); //עובד+
        $auth->add($editTask);

        $deletTask = $auth->createPermission('deletTask'); //מנהל+
        $auth->add($deletTask);

    //-----------------------------------קישור לקובץ חוקים----------------------------
        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);
    //-----------------------------------------חוקים-----------------------------------
                
        $editMyUser->ruleName = $rule->name;                
        $auth->add($editMyUser);                 
                                  
     //-------------------------------------שיוך תפקידים להרשאות------------------------    
        $auth->addChild($employee, $editMyUser); 
        $auth->addChild($employee, $veiwUsers);
        $auth->addChild($employee, $veiwUser);
        $auth->addChild($employee, $createTask); 
        $auth->addChild($employee, $editTask);


        $auth->addChild($admin, $deletTask);
        //-----------------------------קישור בין החוק עם הכובע לבלי הכובע---------------------
        $auth->addChild($editMyUser, $editUser); 
    }

    

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_081757_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_081757_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
