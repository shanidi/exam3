<?php

use yii\db\Migration;

/**
 * Class m180624_054446_init_urgency
 */
class m180624_054446_init_urgency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
           $this->insert('urgency', [
            
            'name' => 'low',
        ]);

        $this->insert('urgency', [
            
            'name' => 'normal',
        ]);
            $this->insert('urgency', [
           
            'name' => 'critical',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_054446_init_urgency cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_054446_init_urgency cannot be reverted.\n";

        return false;
    }
    */
}
