<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\urgency;
use dosamigos\selectize\SelectizeTextInput;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model,'urgency')->dropDownList(ArrayHelper::map(Urgency::find()->asArray()->all(), 'id','name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
